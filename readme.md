# Development Information for SolarCar-Software #


Just some guides to point developers in the right direction.

## Setup environment ##
To develop on our pojects, [node.js](https://nodejs.org/en/) and [typescript](https://www.typescriptlang.org/index.html#download-links) are required. You can simply download them on their website or by the following commands:

As IDE we recommend [Visual Studio Code](https://code.visualstudio.com) and to edit markdown we use [haroopad](http://pad.haroopress.com).


## Tools ##
- [Node.js](https://nodejs.org/en/): Serverside JavaScript runtime built on Chrome V8 engine.
- npm: node-package-manager. Download and management-tool for JavaScript and TypeScript packages (libraries).
- [TypeScript](https://www.typescriptlang.org/index.html#download-links): To JavaScript transpiling, typed language.
- [DefinitelyTyped](http://definitelytyped.org): Repository for typings. Typings need to be installed next to JS-package. 
- [TSLint](https://palantir.github.io/tslint/): Lintingtool for TypeScript-sourcecode.
- [EditorConfig](http://editorconfig.org): Lintingtool to format sourcecode.
- [Angular](https://angular.io): Google powered framework for dynamic web-applications.
- [Bootstrap](http://getbootstrap.com): Frontend component library for HTML, CSS and JS. For Angular [ng-bootstrap](https://github.com/ng-bootstrap/ng-bootstrap).
- [codelyzer](http://codelyzer.com): Lintingtool for Angular-projects. Installed by Angular CLI.
- [TypeDoc](http://typedoc.org): Documentation generator for TypeSript.
- [Jasmine](https://jasmine.github.io): Unit-Test-Framework for clean syntax.
- [Karma](https://jasmine.github.io): Unit-Test-Runner.
- [Protractor](http://www.protractortest.org/#/): E2E-Test-Framework to test communication between frontend and backend.
- [Winston](https://github.com/winstonjs/winston): Logging-tool.
- [Gulp](https://gulpjs.com): Task runner.
- [Plot.ly](https://plot.ly/javascript/): Chart-Toolkit.
- [FontAwesome](http://fontawesome.io): Powerful icon-library.
- [MathJs](http://mathjs.org/): Math and Unit library.

In addition, Visual Studio Code also provides suitable add-ons. You can find them in the left menu bar under Extensions -> Recommended.

## Tutorials

### typescript
- https://tutorialzine.com/2016/07/learn-typescript-in-30-minutes
- http://brianflove.com/2016/11/08/typescript-2-express-node/

### Bootstrap in Angular
- https://codingthesmartway.com/using-bootstrap-with-angular/

### creating npm modules in typescript
- https://www.twilio.com/blog/2017/06/writing-a-node-module-in-typescript.html
- https://codeburst.io/https-chidume-nnamdi-com-npm-module-in-typescript-12b3b22f0724

### Basics for Testing
- http://taco.visualstudio.com/en-us/docs/unit-test-01-primer/

## Cheat Sheets
- [git](https://www.git-tower.com/blog/posts/git-cheat-sheet)
- [node & npm](https://medium.com/@peterchang_82818/npm-cheat-sheet-note-tutorial-publish-config-help-5cd3084200aa)
- [typescript](https://www.sitepen.com/blog/2013/12/31/typescript-cheat-sheet/)
- [angular](https://angular.io/guide/cheatsheet)

## Issue Workflow ##
1. Move issue in Trello from *To Do* to *In Process*
2. Create branch for issue with a name that expresses the meaning
3. Work on issue and commit/push to branch
4. Move issue in Trello to *In Review* and create Merge Request in GitLab or HSBO-Git with name of your branch as title
5. Wait for other developer to review your changes/fix 
6. Reviewer merges or moves issue back to *In Process*
7. Reviewer moves issue to *Done*

